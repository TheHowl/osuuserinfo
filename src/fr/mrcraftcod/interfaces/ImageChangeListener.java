package fr.mrcraftcod.interfaces;

import fr.mrcraftcod.objects.ImageEvent;

public interface ImageChangeListener
{
	/**
	 * Invoked when the image is modified.
	 */
	public void onImageChanged(ImageEvent e);
}
