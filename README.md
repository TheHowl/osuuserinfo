Osu!UserInfo
=========

[Osu!UserInfo](https://osu.ppy.sh/forum/p/3094583) is a simple program that will give you information (and track them) about a Osu! user!

Informations given/tracked
-----------------------
Play count  
Scores  
PP  
Accuracy  
Country  
Rank  
Total hits  
Number of 300s, 100s, 50s  
Number of SS, S, A  

How to help that project?
-----------------------

[If you find any bugs, please report them here](https://bitbucket.org/MrCraftCod/osuuserinfo/issues)

If you want, you can translate this project. [Fork the repository](https://bitbucket.org/MrCraftCod/osuuserinfo/fork) and add your language file in src/resources/lang/
